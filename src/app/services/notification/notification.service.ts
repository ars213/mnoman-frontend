import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr: ToastrService) { }

  success(msg,title = "Success!"){
    this.toastr.success(msg,title);
  }

  error(msg,title = "Error!"){
    this.toastr.error(msg,title);
  }

  info(msg,title = "Info!"){
    this.toastr.info(msg,title);
  }
}
