import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../notification/notification.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private apiUrl = "https://jsonplaceholder.typicode.com/";
  constructor(private http : HttpClient,private notify : NotificationService) {

   }

   get(url){
     return new Promise((resolve,reject) => {
       this.http.get(this.apiUrl + url,{observe : 'response'}).subscribe(result => {
         if(result.ok){
          resolve(result.body);
         }else{
          this.notify.error("Request Failed");
          reject(result);
         }
       }) 
     });
   }

   post(url,data = {}){
    return new Promise((resolve,reject) => {
      this.http.post(this.apiUrl + url,data,{observe : 'response'}).subscribe(result => {
        if(result.ok){
         resolve(result.body);
         this.notify.success("Operation Completed Successfully!");
        }else{
          this.notify.error("Request Failed");
         reject(result);
        }
      }) 
    });
  }
}
